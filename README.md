# FFmpegAndroid

### [FFmpeg官方文档](https://ffmpeg.org/)
### [FFmpeg编译流程](https://github.com/xufuji456/FFmpegAndroid/blob/master/doc/FFmpeg_compile_shell.md)
### [FFmpeg常用命令行](https://github.com/xufuji456/FFmpegAndroid/blob/master/doc/FFmpeg_command_line.md)
### [FFmpeg源码分析](https://github.com/xufuji456/FFmpegAndroid/blob/master/doc/FFmpeg_sourcecode.md)
### [JNI与NDK开发](https://github.com/xufuji456/FFmpegAndroid/blob/master/doc/JNI_develop_practice.md)
### [音视频知识汇总](https://github.com/xufuji456/FFmpegAndroid/blob/master/doc/multimedia_knowledge.md)
### [ijkplayer播放器架构](https://github.com/xufuji456/FFmpegAndroid/blob/master/doc/player_framework.md)

### Joining the group to learn FFmpeg:
![](https://gitee.com/lalalaxiaowifi/pictures/raw/master/%20image/20220107164704.png)

### Joining QQ group to learn FFmpeg:
![](https://gitee.com/lalalaxiaowifi/pictures/raw/master/%20image/20220107164646.png)

### 运行时序图:
![](https://gitee.com/lalalaxiaowifi/pictures/raw/master/%20image/20220107164556.png)

### Preview thumbnail when seeking:
![](https://gitee.com/lalalaxiaowifi/pictures/raw/master/%20image/20220107164718.gif)

